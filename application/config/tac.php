<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * @package Session
 *
 * A list of column counts in the tac
 */
$config['column_count'] = '3,2,1';
