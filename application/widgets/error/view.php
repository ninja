<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
Couldn't load widget <?php echo $this->model->name.': ['.$this->exception->getFile().':'.$this->exception->getLine().'] '.$this->exception->getMessage() ?>
<?php if (!IN_PRODUCTION) {
	print '<pre>'.$this->exception->getTraceAsString().'</pre>';
}
?>
